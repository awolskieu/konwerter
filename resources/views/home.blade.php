<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Konwerter walut</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            html,body { background: #333 }

            #loader {
                position: absolute;
                top:0;
                left:0;
                width: 100%;
                height: 100%;

            }

            svg {
                max-width: 25em;
                border-radius: 3px;

                background: #333;
                fill: none;
                stroke: #222;
                stroke-linecap: round;
                stroke-width: 8%;
                position: relative;
                top:50%;
                left:50%;
                transform: translate(-50%,-50%);
            }

            use {
                stroke: #fff;
                animation: a 2s linear infinite
            }

            @keyframes a { to { stroke-dashoffset: 0px } }
        </style>
    </head>
    <body>
    <noscript>
        <div id="noscript">
            <h3>Włącz obsługę JS, aby zobaczyć stronę</h3>
        </div>
    </noscript>
    <div id="loader">

        <svg viewBox="-2000 -1000 4000 2000">
            <path id="inf" d="M354-354A500 500 0 1 1 354 354L-354-354A500 500 0 1 0-354 354z"></path>
            <use xlink:href="#inf" stroke-dasharray="1570 5143" stroke-dashoffset="6713px"></use>
        </svg>
    </div>
        <div class="flex-center position-ref full-height">

            <div id="message"></div>
            <form action="" method="" id="exchangeForm">
                <input type="number" name="amount" id="amount" required min="0" placeholder="amount">
                <select id="from">

                </select>
                <select id="to">

                </select>
                <button type="submit" id="convert">=</button>
                <input type="text" placeholder="Result" id="result">
            </form>
        </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>

        function getCurrencies() {
            jQuery.get('/api/currencies', function(msg) {
              //  console.log(msg);

               jQuery.each(msg, function(id, el) {
                 //  console.log(el);
                 // elem = '';
                   elem = "<option value='"+el.code+"'>"+el.name+"</option>";
                  jQuery('#from').append(elem);
                   jQuery('#to').append(elem);
               });
            });
        }
        jQuery(document).ready(function($){
            getCurrencies();
           $('#loader').fadeOut();

            $('#convert').click(function(ev){
                ev.preventDefault();
                var from = $('#from').val();
                var to = $('#to').val();
                var amount = $('#amount').val();
                var message = $('#message');
                var api = "{{config('app.url')}}/api/exchange/{from}/{to}/{amount}";
                message.text('').removeClass('warning');

                if (from == to) {
                    message.addClass('warning').text('Waluty muszą się różnić od siebie');
                    return;
                }
                if (amount <= 0) {
                    message.addClass('warning').text('Ilość musi być większa od 0');
                    return;
                }
                console.log('test');
                console.log(api.replace('{from}', from).replace('{to}', to).replace('{amount}', amount));
                $.get(api.replace('{from}', from).replace('{to}', to).replace('{amount}', amount), function(msg) {
                    console.log(msg);
                    $('#result').val(msg);
                });



            })
        });
    </script>
    </body>
</html>
