<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use Illuminate\Http\Request;

class ExchangeController extends Controller
{
    /**
     * return list of all currencies
     *
     * @param $code
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function currencies()
    {
        return response()->json(Currency::get(['code', 'name']));
    }

    /**
     * Return info about currency
     *
     * @param $code
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public  function currency($code)
    {
        return \response()->json(Currency::where('code', strtolower($code))->firstOrFail());
    }

    /**
     * return value
     *
     * @param $from
     * @param $to
     * @param int $amount
     *
     * @return string
     */
    public function exchange($from, $to,int $amount)
    {
        $currencyFrom = Currency::where('code', strtolower($from))->firstOrFail();
        $currencyTo = Currency::where('code', strtolower($to))->firstOrFail();

        $value =  $currencyFrom->value;

        $value /=  $currencyTo->value;

        return  number_format($value * $amount, 2);
    }
}
