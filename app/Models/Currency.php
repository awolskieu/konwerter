<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    //
    protected $fillable = ['code', 'name', 'value'];
    public $timestamps = false;
    protected $table = 'currency';
}
