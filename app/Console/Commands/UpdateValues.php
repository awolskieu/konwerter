<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use \App\Models\Currency;

class UpdateValues extends Command
{
    /**
     * Base link of the API
     *
     * @var string
     */
    protected $uri = "http://api.nbp.pl/api/exchangerates/tables/A/?format=json";

    /**
     * API params
     *
     * @var array
     */
   // protected $tables = ['A', 'B', 'C'];


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateValues';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update currencies values based on NBP data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $client = new Client();


            $res = $client->get($this->uri); //call to API and get the data

            if ($res->getStatusCode() == 200) { //check if data exists 
                $body = json_decode($res->getBody());
                $curriences = $body[0]->rates;
                foreach ($curriences as $currency) {
                    Currency::updateOrCreate(
                        ['code' => $currency->code, 'name'=>$currency->currency],
                        ['value' => convertTo($currency->mid, 4)]
                    ); //update value or create new data instance
                }

            }

    }
}
