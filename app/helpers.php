<?php

/**
 * get integer from float without loss
 *
 * @param $value
 * @param $precision
 * @param int $round
 *
 * @return int
 */
function convertTo ($value, $precision, $round = 0)
{
    $value = $round > 0 ? round($value, $round) : $value; //get clear number

    return (int) ($value * pow(10 , $precision));
}

/**
 * convert from integer to float without loss
 * @param $value
 * @param $precision
 *
 * @return float|int
 */
function convertFrom($value, $precision)
{
    return $value * pow(10, $precision*-1);
}